const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports={
    entry: ['./src/js/index.js','babel-polyfill'],
    output:{
        path: path.resolve(__dirname,'dist'),
        filename: 'js/bundle.js'
    },
    devServer:{
        contentBase: "./dist"
    },
    plugins: [
        new HtmlWebpackPlugin({
            filename:"index.html",
            template: "./src/index.html"
        })
    ],
    module:{
        rules:[
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use:{
                    loader: 'babel-loader'
                }
            }
        ]
    }
};

// path is addon to enter absolute path
// __dirname is absolute path of current project folder,field path require absolute path
// ./ means current folder
// mode:'development'  will not compress or modify code \\ назначение можно перенести в package json в сам скрипт --mode development

// in order to use webpack command line should do 'npm i webpack-cli --save-dev'

// 1 .in order to use dev-server it should be installed: npm i webpack-dev-server --save-dev
// 2/ devserver setup in webpack.config
// devServer:{contentBase: "./dist"}  // sets the folder with files to launch
// 3. in package.json add script: "start":"webpack-dev-server --mode development --open" --open means to open page automatically

// 1. npm i html-webpack-plugin --save-dev
// 2. in package.json  //const HtmlWebpackPlugin = require('html-webpack-plugin');
// 3.
/* plugins: [
        new HtmlWebpackPlugin({
            filename:"index.html", //how to name new html
            template: "./src/index.html" //where to get original html
        })
    ]
*/
//4 функция выше !не сохраняет новый файл на диске! файл схраняется на виртуальном сервере, команды dev или build сохраняют


/*
// BABEL:  npm i babel-core babel-preset-env babel-loader --save-dev
Нужно создать файл .babelrc с настройками внутри (см пример в проекте)
inside webpack config
 module:{
        rules:[
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use:{
                    loader: 'babel-loader'
                }
            }
        ]
    }

   бабель конвертирует не все т.к. не все вещи из ес6 есть в ес5, к примеру Промисы
   npm i babel-polyfill --save    -сохраняется на проде,добавляет новый код который решает проблему выше
   also should be added to entry in webpckconfig:    entry: ['./src/js/index.js','babel-polyfill'], (where first element was set before and babel is added)
* */
