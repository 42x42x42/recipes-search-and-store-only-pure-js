import 'babel-polyfill';
import {elements} from "./base";

//get value of input field
export const getInput = () => elements.searchInput.value;

//clear input field
export const clearInput = () => {
    elements.searchInput.value = '';
};

//clear result list
export const clearResults = () => {
    elements.searchResList.innerHTML = '';
    elements.searchResPages.innerHTML='';
};
export const highlightSelected = id => {
    const resultsArr = document.querySelectorAll('.results__link');
    resultsArr.forEach(el => el.classList.remove('results__link--active'));

    document.querySelector(`.results__link[href*="${id}"]`).classList.add('results__link--active');
};

//limit length of recipe title
const limitRecipeTitle = (title, limit = 17) => {
    let newTitle = [];
    if (title.length > limit) {
        title.split(' ').reduce((acc, cur) => {
            if (acc + cur.length < limit) {
                newTitle.push(cur);
            }
            return acc + cur.length;
        }, 0);
        return `${newTitle.join(' ')} ...`;
    }
    return title;
};

//create and render single recipe html element
const renderRecipe = recipe => {
    let markup = `
                <li>
                     <a class="results__link" href="#${recipe.recipe_id}">
                        <figure class="results__fig">
                            <img src="${recipe.image_url}" alt="${recipe.title}">
                        </figure>
                        <div class="results__data">
                            <h4 class="results__name">${limitRecipeTitle(recipe.title)}</h4>
                            <p class="results__author">${recipe.publisher}</p>
                        </div>
                    </a>
                </li>
    `;
    elements.searchResList.insertAdjacentHTML('beforeend', markup);
};

// creates page button for paggination of recipes, return string with markup
// accept current page number and type of button 'prev' or 'next'
const createButton = (page, type) => `
                <button class="btn-inline results__btn--${type}" data-goto=${type === 'prev' ? page - 1 : page + 1}>
                    <span>Page ${type === 'prev' ? page - 1 : page + 1}</span>
                    <svg class="search__icon">
                        <use href="img/icons.svg#icon-triangle-${type === 'prev' ? 'left' : 'right'}"></use>
                    </svg>
                    
                </button>
`;


const renderPageButtons = (page, numResult, resPerPage) => {
    //max number of pages
    const pages = Math.ceil(numResult / resPerPage);

    let button;

    if (page === 1 && pages > 1) {
        // show only next button
        button = createButton(page, 'next');

    }
    else if (page < pages) {
        // show both buttons
        button = `
        ${button = createButton(page, 'next')}
        ${button = createButton(page, 'prev')}
        `
    }

    else if (page === pages && pages > 1) {
        // show only prev button
        button = createButton(page, 'prev');
    }

    elements.searchResPages.insertAdjacentHTML('afterbegin', button);
};

//render list of recipes
// accept current page and results per page
export const renderResult = (recipes, page = 1, resPerPage = 10) => {
    //render search results of current page
    let start = (page - 1) * resPerPage;
    let end = page * resPerPage;
    recipes.slice(start, end).forEach(renderRecipe);

    //render buttons
    renderPageButtons(page, recipes.length, resPerPage)

};
