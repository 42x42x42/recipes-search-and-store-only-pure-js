// GLOBAL APP CONTROLLER FILE
import 'babel-polyfill';  //async doesn't work without this import
import Search from './models/Search';
import Recipe from './models/Recipe';
import List from './models/List';
import Likes from './models/Likes';
import * as searchView from './views/searchView';
import * as recipeView from './views/recipeView';
import * as listView from './views/listView';
import * as likesView from './views/likesView';
import {elements, renderLoader, clearLoader} from "./views/base";


/*GLOBAL STATE OF THE APP
*  - search object
*  - current recipe obj
*  - shopping list obj
*  - liked recipes
*/

// !store all states of app!
const state = {};
// window.s = state; // for TESTING


/* **********************************************
*********SEARCH CONTROLLER**********************
*********************************************** */

// launch, search and display data from search input
const controlSearch = async () => {

    // 1. get query from view
    let query = searchView.getInput();

    if (query) {
        // 2. new search obj and add result to state
        state.search = new Search(query);

        // 3. prepare UI for results
        searchView.clearInput();
        searchView.clearResults();
        renderLoader(elements.searchRes);

        try {
            // 4. search for recipes
            await state.search.getResults(); // store?

            // 5. render results on UI
            clearLoader(); // delete loader spinner
            searchView.renderResult(state.search.result);

        }
        catch (err) {
            console.log(err);
            clearLoader(); // delete loader spinner
        }
    }
};

elements.searchForm.addEventListener('submit', e => {
    e.preventDefault(); //prevent default page reloading after submit
    controlSearch(); //launch search

});

elements.searchResPages.addEventListener('click', e => {
    //event delegation: buttons do not exist when page loaded 1st time, so event is on their parent. After click event bubles up to parrent element
    let btn = e.target.closest('.btn-inline');

    if (btn) {
        // new page number is stored in html-5 element in data-goto, access via dataset property
        let goToPage = +btn.dataset.goto;
        searchView.clearResults();
        searchView.renderResult(state.search.result, goToPage);
    }

});


/* **********************************************
*********RECIPE CONTROLLER**********************
*********************************************** */

const controlRecipe = async () => {
    //get id from hash in url
    let id = +window.location.hash.replace('#', '');

    if (id) {
        // prepare UI for changes
        recipeView.clearRecipe();
        renderLoader(elements.recipe);

        //highlight selected recipe in a list
        if (state.search) searchView.highlightSelected(id);

        // create new recipe object
        state.recipe = new Recipe(id);

        // fulfill new recipe object with formatted ingredients list
        try {
            // get recipe data from API
            await state.recipe.getRecipe();
            state.recipe.parseIngredients();

            // calculate servings and time
            state.recipe.calcServings();
            state.recipe.calcTime();

            // render recipe
            clearLoader();
            recipeView.renderRecipe(
                state.recipe,
                state.likes.isLiked(id)
            );
        } catch (err) {
            alert(err);
        }
    }
};

// 'hashchange' shows recipe view after clicking on one
// 'load' event will request recipe data from API again if page was reloaded
['hashchange', 'load'].forEach(event => window.addEventListener(event, controlRecipe));


/* **********************************************
*********SHOPPING LIST CONTROLLER*****************
*********************************************** */

const controlList = () => {
    // create new list if there is none
    if (!state.list) state.list = new List();

    //add items
    state.recipe.ingredients.forEach(ing => {
        //push ingredient into list and return it into const item
        const item = state.list.addItem(ing.count, ing.unit, ing.ingredient);
        //add ingredient to UI
        listView.renderItem(item);
    })

};

// handle buttons clicks in SHOPPING LIST
// handle delete and update list item events
elements.shoppingList.addEventListener('click', e => {
    //closest find closest parent or the target element which matches selector
    const id = e.target.closest('.shopping__item').dataset.itemid;
    if (e.target.matches('.shopping__delete, .shopping__delete *')) {
        //delete from state
        state.list.deleteItem(id);
        //delete from UI
        listView.deleteItem(id);
    }
    // if click is on the input with ingredients count
    if (e.target.matches(".shopping__count-value")) {
        const val = parseFloat(e.target.value);
        state.list.updateCount(id, val);
    }
});

/* **********************************************
***************LIKES CONTROLLER*****************
*********************************************** */


const controlLike = () => {
    if (!state.likes) {
    //
    }
    const currentID = state.recipe.id;

    //recipe has NOT been liked
    if (!state.likes.isLiked(currentID)) {
        // add like to the state
        const newLike = state.likes.addLike(
            currentID,
            state.recipe.title,
            state.recipe.author,
            state.recipe.img
        );

        // toogle like button
        likesView.toggleLikeBtn(true);

        // add to UI
        likesView.renderLike(newLike);

        // reicpe HAVE BEEN liked
    } else {
        // remove like from the state
        state.likes.deleteLike(currentID);

        // toogle like button
        likesView.toggleLikeBtn(false);

        // remove from UI
        likesView.deleteLike(currentID);
    }

    likesView.toggleLikeMenu(state.likes.getNumLikes());
};


//restore liked recipes on page load
window.addEventListener('load', () => {
    state.likes = new Likes();
    //restore likes
    state.likes.readStorage();
    //toggle button with likes list
    likesView.toggleLikeMenu(state.likes.getNumLikes());

    //render the existing likes
    state.likes.likes.forEach(like => likesView.renderLike(like))
});

// handle recipe buttons clicks in RECIPE
// click on button decrease and increase servings, at the moment of eventlistener creation they do not exist
elements.recipe.addEventListener('click', e => {
    if (e.target.matches('.btn-decrease, .btn-decrease *')) {
        // btn decrease is clicked
        if (state.recipe.servings > 1) {
            state.recipe.updateServings('dec');
            recipeView.updateServingIngredients(state.recipe);
        }
    } else if (e.target.matches('.btn-increase, .btn-increase *')) {
        // btn increase is clicked
        state.recipe.updateServings('inc');
        recipeView.updateServingIngredients(state.recipe);
    } else if (e.target.matches('.recipe__btn--add, .recipe__btn--add *')) {
        //add ingredients to shopping list
        controlList();
    } else if (e.target.matches('.recipe__love, .recipe__love *')) {
        //like controller
        controlLike();
    }
});



