import axios from 'axios'; // // npm i axios --save  substitute fetch, supported by all browers,return JSON
import 'babel-polyfill';

import {APIkey,proxy} from '../config';

// request data from API with recipes containing query
export default class Search {
    constructor(query) {
        this.query = query;
        this.result=[];
    }

    //public method to get request results
    async getResults() {
        try {
            const res = await axios(`${proxy}http://food2fork.com/api/search?key=${APIkey}&q=${this.query}`);
            this.result = res.data.recipes;
        } catch (error) {
            console.error(error);
        }
    }
}

