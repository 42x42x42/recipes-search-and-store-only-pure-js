import axios from 'axios'; // // npm i axios --save  substitute fetch, supported by all browers,return JSON
import 'babel-polyfill';

import {APIkey, proxy} from '../config';

export default class Recipe {
    constructor(id) {
        this.id = id;
    };

    async getRecipe() {
        try {
            const res = await axios(`${proxy}http://food2fork.com/api/get?key=${APIkey}&rId=${this.id}`);
            this.title = res.data.recipe.title;
            this.author = res.data.recipe.publisher;
            this.img = res.data.recipe.image_url;
            this.url = res.data.recipe.source_url;
            this.ingredients = res.data.recipe.ingredients;
            // this.time = '';
            // this.servings = '';
        } catch (error) {
            console.error(error);
        }
    };


    // format ingredients and split into name,units, amount
    parseIngredients() {
        const unitsLong = ['tablespoons', 'tablespoon', 'ounces', 'ounce', 'teaspoons', 'teaspoon', 'cups', 'pounds'];
        const unitsShort = ['tbsp', 'tbsp', 'oz', 'oz', 'tsp', 'tsp', 'cup', 'pound'];
        const units = [...unitsShort, 'kg', 'g'];

        // go through all list of all ingredients and return new formated list
        const newIngredients = this.ingredients.map(el => {
            // 1) Uniform units
            let ingredient = el.toLowerCase();
            //loop through all unitLong to replace them in ingredient
            unitsLong.forEach((unit, i) => {
                ingredient = ingredient.replace(unit, unitsShort[i]);
            });
            // 2) Remove parentheses
            ingredient = ingredient.replace(/ *\([^)]*\) */g, ' ');
            // 3) Parse ingredients into amount, unit and ingredient
            const arrIng = ingredient.split(' ');
            const unitIndex = arrIng.findIndex(el2 => units.includes(el2));

            let objIng; // final object with ingredients count,unit,name
            if (unitIndex > -1) {
                //case if there is a unit
                const arrCount = arrIng.slice(0, unitIndex); // slice piece till unit declaration
                let count;
                if (arrCount.length === 1) {
                    count = +((eval(arrIng[0].replace('-', '+'))).toFixed(2));
                }
                else {
                    count = +((eval(arrIng.slice(0, unitIndex).join('+'))).toFixed(2));
                }
                //create ingredient object
                objIng = {
                    count: count,
                    unit: arrIng[unitIndex],
                    ingredient: arrIng.slice(unitIndex + 1).join(' ')
                };
            } else if (parseInt(arrIng[0], 10)) {
                // case if there is NO unit but 1st element is a number
                objIng = {
                    count: parseInt(arrIng[0], 10),
                    unit: '',
                    ingredient: arrIng.slice(1).join(' ')
                };
            } else if (unitIndex === -1) {
                //case if there is NO unit
                objIng = {
                    count: 1,
                    unit: '',
                    ingredient: ingredient
                };
            }
            // return formated ingredients list
            return objIng;
        });

        this.ingredients = newIngredients;
    };


    //calc time for recipe cooking
    calcTime() {
        // assuming that we need 5min for each ingredient
        this.time = this.ingredients.length * 5;
    };

    // calc portions in a recipe
    calcServings() {
        //hardcoded data due to current lack of info in API
        this.servings = 4;
    };

    updateServings(type) {
        const newServings = type === 'dec' ? this.servings - 1 : this.servings + 1;

        this.ingredients.forEach(ing => {
            ing.count = ing.count *(newServings / this.servings);
        });

        this.servings = newServings;
    };
};